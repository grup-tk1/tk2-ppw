from django.db import models

# Create your models here.

CHOICES= [
    ('Full Stack Developer', 'Full Stack Developer'),
    ('Front-End Developer', 'Front-End Developer'),
    ('Back-End Developer', 'Back-End Developer'),
    ]
class User(models.Model):
    name= models.CharField(max_length=100)
    role= models.CharField(max_length=100, choices= CHOICES)
