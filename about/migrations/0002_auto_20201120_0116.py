# Generated by Django 3.1.3 on 2020-11-19 18:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='fruit',
            field=models.CharField(choices=[('Full Stack Developer', 'Full Stack Developer'), ('Front-End Developer', 'Front-End Developer'), ('Back-End Developer', 'Back-End Developer')], max_length=100),
        ),
    ]
