from django import forms  
from .models import User


CHOICES= [
    ('Full Stack Developer', 'Full Stack Developer'),
    ('Front-End Developer', 'Front-End Developer'),
    ('Back-End Developer', 'Back-End Developer'),
    ]

class UserForm(forms.ModelForm):
    name= forms.CharField(max_length=100)
    role= forms.CharField(widget=forms.Select(choices=CHOICES))
    class Meta:
        model = User
        fields =['name','role']