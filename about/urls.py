from django.urls import path, include
from .views import index,form,savekegi


app_name = "about"
urlpatterns = [
    path('about/', index, name='index'),
    path('form/', form, name='form'),
    path('form/savekegi',savekegi),
]
