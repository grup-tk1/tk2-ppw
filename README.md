## TK2 COVID 19 CegahCovid

# Kelompok
Nama Anggota Kelompok:
- Mario Serano 
- Ibnu Hambali
- Muhammad Irza
- Maximillian Aryo

# Link Herokuapp
Herokuapp:
tk1-covid.herokuapp.com

# Tentang Aplikasi
Aplikasi yang telah kami buat adalah aplikasi yang bertujuan untuk menaikkan awareness masyarakat terhadap COVID-19. Kami menyediakan portal artikel-artikel seputar COVID-19 dengan harapan portal kami dapat digunakan untuk kepentingan masyarakat umum.

# Fitur to be implemented:

- Authentication  
- Article Listing 
- Search for Article
- User Review for Article