from django.shortcuts import render
from article.models import Article
# Create your views here.
def index(request):
    articles = Article.objects.all()
    context = {'articles' : articles}
    return render(request, 'landing_page/index.html', context)

def detail(request, pk):
    article = Article.objects.get(id=pk)
    context = {'article' : article}
    return render(request, 'article/detail.html', context)