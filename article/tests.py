from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from article.models import Account
from .views import *
from .models import Article

# Create your tests here.

class ArticleTest(TestCase):
#Model_test
    def test_model_can_create_article(self):
        Account.objects.create_user(
            username="username",
            email="email.@mail.com",
            nama_depan="namadepan",
            nama_belakang="namabelakang"
        )
        user = Account.objects.get(id=1)
        Article.objects.create(
            topik="corona",
            judul="coronamenular",
            image="lalalala.jpeg",
            content="content",
            author=user
        )

#url_test
    def test_article_login_url_is_exist(self):
        response = Client().get('/article/')
        self.assertEqual(response.status_code, 200)

#test_fungsi_index
    def test_article_using_index_func(self):
        found = resolve('/article/')
        self.assertEqual(found.func, index)


#test_pakai_template
    def test_article_using_index_template(self):
        response = Client().get('/article/')
        self.assertTemplateUsed(response, 'article/index.html')

# test for article search
    def test_article_search_url_is_exist(self):
        response = Client().get('/article/search/')
        self.assertEqual(response.status_code, 200)

    def test_search_using_search_func(self):
        found = resolve('/article/search/')
        self.assertEqual(found.func, get_search)
    
    def test_search_using_search_template(self):
        response = Client().get('/article/search/')
        self.assertTemplateUsed(response, "article/search.html")
