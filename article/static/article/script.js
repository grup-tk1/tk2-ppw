$('.btn-add-article').click(() => { // onClick Event
    $.post($("meta[name='url_add']").attr("content"), {
            topik: $("#id_topik").val(),
            judul: $("#id_judul").val(),
            image: $("#id_image").val(),
            content: $("#id_content").val(),
        })
        .done((res) => {
            $("#id_topik").val("")
            $("#id_judul").val("")
            $("#id_image").val("")
            $("#id_content").val("")

            window.location.replace(`${$("meta[name='url_article']").attr("content")}detail/${res.id}`);
        })
        .fail((e) => console.log(e.responseText))
})

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

$(document).ready(() => {
    $('#id_judul').change(() => { // onChange event
        let text = "Judul Artikel : "
        $('#judul').html(text + $('#id_judul').val())
    })
    $(document).scroll(() => {
        $('nav').attr('style', `background-color: ${getRandomColor()} !important`);
    })
})