$('.btn-add-review').click(() => {
    $.post($("meta[name='url_add']").attr("content"), {
            csrfmiddlewaretoken: $("meta[name='csrf']").attr("content"),
            score: $("#score").val(),
        })
        .done(() => {
            getData()
        })
        .fail((e) => console.log(e.responseText))
})
$(document).ready(() => getData())

// Functions

function generateRow(data, i) {
    review = data
    return `<tr>
    <th scope="row">${i+1}</th>
    <td>${review.user}</td>
    <td>${review.score}</td>
  </tr>
`
}

function getData() {
    console.log($("meta[name='url_get']").attr("content"));

    $.get($("meta[name='url_get']").attr("content"))
        .done((results) => {
            let content = $("tbody")
            content.empty()
            if (results.result.length == 0) {
                content.prev().empty()
                content.append("<h4 class='mx-3'>No one review this article, be the first</h4>")
            } else {
                let articles = results.result
                articles.forEach((a, i) => {
                    content.append(generateRow(a, i))
                })
            }
        })
        .fail((e) => console.log(e.responseText))
}