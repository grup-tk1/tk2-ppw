$('document').ready(() => {
    function login_ajax(){
        let data = {
            email:$("#email").val(),
            password:$("#password").val()
        }
    
        $("#form-login")[0].reset()
        toastr["info"]("Please wait...")
        $.ajax({
            url: "{% url 'login_endpoint' %}",
            type:"POST",
            data:{...data, csrfmiddlewaretoken:"{{ csrf_token }}"},
            success: function(json){
                const { success } = json
                if(success){
                    toastr["success"]("Login successful, soon you will be redirected.")
                    setTimeout(() => {
                        window.location.pathname = "/"
                    }, 1000)
                }
            },
            error: function(xhr, errmsg, err){
                const { responseJSON } = xhr
                if(!responseJSON.success){
                    toastr["error"]("Information credentials does not match, please try again.")
                }
            }
        })
    }
    
    $("#form-login").on("submit", (event) => {
        event.preventDefault()
        login_ajax()
    })
})