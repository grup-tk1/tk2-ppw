$('document').ready(() => {
    function create_account(){
        let data = {
            nama_depan:$('#nama_depan').val(),
            nama_belakang:$('#nama_belakang').val(),
            email:$('#email').val(),
            username:$('#username').val(),
            password:$('#password').val(),
            confirm_password:$('#confirm_password').val()
        }
        $.ajax({
            url: "{% url 'register_endpoint' %}",
            type: "POST",
            data: {...data, csrfmiddlewaretoken: "{{ csrf_token }}"},
            success: function(json){
                const { success } = json
                if(success){
                    toastr["success"]("Successfully created account, you will be redirected")
                    setTimeout(() => {
                        window.location.pathname = "/"
                    }, 1000)
                }
            },
            error: function(xhr, errmsg, err){
                const { responseJSON } = xhr
                if(!responseJSON.success){
                    toastr["error"]("Something is wrong, please reload and try again!")
                }
            }
        })
    }
    $("#register-form").on("submit", (event) => {
        event.preventDefault()
        create_account()
    })
})
