from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# So many boilerplates i hate this D:
class MyAccountManager(BaseUserManager):
    # Parameters are USERNAME_FIELD and REQUIRED_FIELDS
    def create_user(self, email, username, nama_depan, nama_belakang, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have an username")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            nama_depan=nama_depan,
            nama_belakang=nama_belakang,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, username, password, nama_depan, nama_belakang):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
            nama_depan=nama_depan,
            nama_belakang=nama_belakang
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        
        return user

class Account(AbstractBaseUser):
    #Custom Fields
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    nama_depan = models.CharField(verbose_name="nama_depan", max_length=30)
    nama_belakang = models.CharField(verbose_name="nama_belakang", max_length=30)

    # Required Fields
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(verbose_name="date_joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last_login", auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    # How does the user login?
    USERNAME_FIELD = 'email'

    # Set Required Fields
    REQUIRED_FIELDS = ['username', "nama_depan", "nama_belakang"]

    # How to use the manager?
    objects = MyAccountManager()

    def __str__(self):
        return self.username
    
    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True
