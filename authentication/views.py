from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

from .forms import UserLoginForm, UserRegisterForm

# Views definitions
def login_view(request):
    if request.user.is_authenticated:
        return redirect("/")

    # Redirect user, hidden input, next
    form = UserLoginForm()

    context = {
        'form': form,
    }

    return render(request, "authentication/login.html", context)


def register_view(request):
    if request.user.is_authenticated:
        return redirect("/")

    next = request.GET.get('next')
    form = UserRegisterForm()

    context = {
        'form': form,
    }
    return render(request, "authentication/register.html", context)


def logout_view(request):
    logout(request)
    return redirect('/')

# Endpoints definitions
def login_endpoint(request):
    if request.user.is_authenticated or request.method != 'POST':
        return HttpResponse(status=400)

    form = UserLoginForm(request.POST)

    if form.is_valid():
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')

        user = authenticate(email=email, password=password)
        login(request, user)

        return JsonResponse({"success": True}, status=200)

    return JsonResponse({"success": False}, status=400)


def register_endpoint(request):
    if request.user.is_authenticated or request.method != "POST":
        return HttpResponse(status=400)

    form = UserRegisterForm(request.POST)

    if form.is_valid():
        user = form.save(commit=False)  # False means don't save it yet
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(email=user.email, password=password)
        login(request, user)

        return JsonResponse({"success": True}, status=200)
    return JsonResponse({"success": False})


def logout_endpoint(request):
    if not request.user.is_authenticated:
        return HttpResponse(status=400)
    logout(request)
    return JsonResponse({'success': True}, status=200)