from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register_view, name="register_view"),
    path('', views.login_view, name="login_view"),
    path('logout/', views.logout_view, name="logout_view"),
    path('login-endpoint/', views.login_endpoint, name="login_endpoint"),
    path('register-endpoint/', views.register_endpoint, name="register_endpoint"),
    path('logout-endpoint/', views.logout_endpoint, name="logout_endpoint")
]
