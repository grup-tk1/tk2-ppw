from django import forms
from django.contrib.auth import (
    authenticate,
    get_user_model
)

User = get_user_model()


class UserLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'placeholder': 'email', 'id': 'email'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'password', 'id': 'password'}))

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            user = authenticate(email=email, password=password)
            if not user:
                raise forms.ValidationError("Wrong credentials!")
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(
        label='Email', widget=forms.EmailInput(attrs={'id': 'email'}))
    password = forms.CharField(
        label='Password', widget=forms.PasswordInput(attrs={'id': 'password'}))
    nama_depan = forms.CharField(
        label='Nama Depan', widget=forms.TextInput(attrs={'id': 'nama_depan'}))
    nama_belakang = forms.CharField(
        label='Nama Belakang', widget=forms.TextInput(attrs={'id': 'nama_belakang'}))
    confirm_password = forms.CharField(
        label='Confirm Password', widget=forms.PasswordInput(attrs={'id': 'confirm_password'}))
    username = forms.CharField(
        label='Username', widget=forms.TextInput(attrs={'id': 'username'}))

    class Meta:
        model = User
        fields = [
            'nama_depan',
            'nama_belakang',
            'email',
            'username',
            'password',
        ]

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        username = self.cleaned_data.get('username')
        confirm_password = self.cleaned_data.get('confirm_password')

        if password != confirm_password:
            raise forms.ValidationError('Password must match!')

        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError(
                "This email has already been registered")

        username_qs = User.objects.filter(username=username)
        if username_qs.exists():
            raise forms.ValidationError(
                "This user has already been registered")

        return super(UserRegisterForm, self).clean(*args, **kwargs)
