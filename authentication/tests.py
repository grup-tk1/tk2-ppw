from django.test import TestCase, Client
from django.forms import forms
from django.urls import resolve
from django.apps import apps
from .views import (login_view, register_view, logout_view,
                    login_endpoint, register_endpoint, logout_endpoint)
from .models import Account
from .forms import (
    UserLoginForm,
    UserRegisterForm
)
from .apps import AuthenticationConfig

# Create your tests here.


class AuthenticationTest(TestCase):

    # URL Routing Test
    def test_authentication_register_url_is_exist(self):
        response = Client().get('/authentication/register/')
        self.assertEqual(response.status_code, 200)

    def test_authentication_login_url_is_exist(self):
        response = Client().get('/authentication/')
        self.assertEqual(response.status_code, 200)

    def test_authentication_logout_url_is_exist(self):
        response = Client().get('/authentication/logout/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_authentication_using_register_template(self):
        response = Client().get('/authentication/register/')
        self.assertTemplateUsed(response, 'authentication/register.html')

    def test_authentication_using_index_template(self):
        response = Client().get('/authentication/')
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_authentication_using_register_func(self):
        found = resolve('/authentication/register/')
        self.assertEqual(found.func, register_view)

    def test_authentication_using_index_func(self):
        found = resolve('/authentication/')
        self.assertEqual(found.func, login_view)

    def test_authentication_using_logout_func(self):
        found = resolve('/authentication/logout/')
        self.assertEqual(found.func, logout_view)

    def test_authentication_using_login_endpoint_func(self):
        found = resolve('/authentication/login-endpoint/')
        self.assertEqual(found.func, login_endpoint)

    def test_authentication_using_register_endpoint_func(self):
        found = resolve('/authentication/register-endpoint/')
        self.assertEqual(found.func, register_endpoint)

    def test_authentication_using_logout_endpoint_func(self):
        found = resolve('/authentication/logout-endpoint/')
        self.assertEqual(found.func, logout_endpoint)

    # Model Test
    def test_model_can_create_account(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        count = Account.objects.count()
        self.assertEqual(1, count)

    def test_model_no_email_create_account(self):
        try:
            new_account = Account.objects.create_user(
                email=None,
                username="test_user",
                nama_depan="testdepan",
                nama_belakang="testbelakang",
                password="password"
            )
            self.fail("error didnt raise")
        except ValueError:
            pass

    def test_model_no_username_create_account(self):
        try:
            new_account = Account.objects.create_user(
                email="test@gmail.com",
                username=None,
                nama_depan="testdepan",
                nama_belakang="testbelakang",
                password="password"
            )
            self.fail("error didnt raise")
        except ValueError:
            pass

    def test_model_create_superuser(self):
        new_account = Account.objects.create_superuser(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        count = Account.objects.count()
        self.assertEqual(1, count)

    def test_model_can_account_str(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        self.assertEqual(str(new_account), new_account.username)
        self.assertEqual(new_account.has_perm("a", "b"), new_account.is_admin)
        self.assertEqual(new_account.has_module_perms("authentication"), True)

    def test_apps_py(self):
        self.assertEqual(AuthenticationConfig.name, 'authentication')
        self.assertEqual(apps.get_app_config(
            'authentication').name, 'authentication')

    def test_can_register(self):
        data = {
            "email": "test@gmail.com",
            "nama_depan": "testdepan",
            "nama_belakang": "testbelakang",
            "username": "testing",
            "password": "password",
            "confirm_password": "password"
        }
        response = Client().post('/authentication/register-endpoint/', data=data)

        count = Account.objects.count()

        self.assertEqual(1, count)

        self.assertEqual(response.status_code, 200)

    def test_can_login(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        response = Client().post("/authentication/login-endpoint/", data=data)

        self.assertEqual(response.status_code, 200)

    # Complex logics
    def test_loggedin_views_login(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.get('/authentication/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_loggedin_views_register(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.get('/authentication/register/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_login_endpoint_loggedin(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.post(
            '/authentication/login-endpoint/', data=data)

        self.assertEqual(response.status_code, 400)

    def test_clean_user_login_form_not_exist(self):
        form = UserLoginForm(
            {"email": "test@gmail.com", "password": "password"})
        try:
            form.is_valid()
            form.clean()
            self.fail("user found despite not made yet")
        except forms.ValidationError:
            pass

    def test_clean_user_register_form_password_not_equal(self):
        register_data = {
            "email": "test@gmail.com",
            "username": "test_user",
            "nama_depan": "testdepan",
            "nama_belakang": "testbelakang",
            "password": "password",
            "confirm_password": "pssaword"
        }
        form = UserRegisterForm(register_data)
        try:
            form.is_valid()
            form.clean()
            self.fail("form password not equal but not raising error")
        except forms.ValidationError:
            pass

    def test_clean_user_register_form_email_already_exits(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        register_data = {
            "email": "test@gmail.com",
            "username": "test_user",
            "nama_depan": "testdepan",
            "nama_belakang": "testbelakang",
            "password": "password",
            "confirm_password": "password"
        }
        form = UserRegisterForm(register_data)
        try:
            form.is_valid()
            form.clean()
            self.fail("form email already exist but not raising error")
        except forms.ValidationError:
            pass

    def test_clean_user_register_form_username_already_exits(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        register_data = {
            "email": "testie@gmail.com",
            "username": "test_user",
            "nama_depan": "testdepan",
            "nama_belakang": "testbelakang",
            "password": "password",
            "confirm_password": "password"
        }
        form = UserRegisterForm(register_data)
        try:
            form.is_valid()
            form.clean()
            self.fail("form username already exist but not raising error")
        except forms.ValidationError:
            pass

    def test_register_endpoint_loggedin(self):
        register_data = {
            "email": "test@gmail.com",
            "username": "test_user",
            "nama_depan": "testdepan",
            "nama_belakang": "testbelakang",
            "password": "password"
        }
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.post(
            '/authentication/register-endpoint/', data=register_data)

        self.assertEqual(response.status_code, 400)

    def test_register_endpoint_get(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.get(
            '/authentication/register-endpoint/')

        self.assertEqual(response.status_code, 400)

    def test_login_endpoint_get(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email": "test@gmail.com",
            "password": "password"
        }

        self.client.post(
            "/authentication/login-endpoint/", data=data)

        response = self.client.get(
            '/authentication/login-endpoint/')

        self.assertEqual(response.status_code, 400)
